# API Mongo DB, Redis, JWT

This project is a REST API made with node JS. It permits to create an user, log to the API, create a task et return user's tasks.
 
## Needs

### Mongo DB
To run this project you will need a mongo db database, with these collection : task and user

### Redis
This project use Redis as a cache to invalidate used JWT token

### Bcrypt
Bcrypt is used to hash password before store them in the DB
## Start

Clone the repo and install packages with ```npm install```

## Files architecture
Entry point is ```index.js```. Express app is defined is ```server.js```.
Router is in ```/app/router.js```
## Test

```npm test```