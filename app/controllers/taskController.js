const Task = require("../models/task");
const jwt = require('../auth/jwt');
const dbCache = require('../auth/cache');

module.exports = {
  async getAllTasks(_, res) {
    try {
      const tasks = await Task.find();
      if (!tasks) {
        res.status(404).json("Tasks not found");
      }
      res.json(tasks);
    } catch (error) {
      res.status(500).json({ error: error });
    }
  },

  async getAllTasksOneUser(req, res) {
    try {
      const {userId} = req.params;
      const tasks = await Task.find({owner: userId});
      const token = req.headers.authorization.replace('Bearer ', '');
      const infos = JSON.parse(Buffer.from(token.split('.')[1], 'base64').toString());
      const newToken = jwt.makeToken(infos.data);
      await dbCache.set("user-0"+infos.data, newToken, {EX: 4*60*60, NX: false});
      if (!tasks) {
        res.status(404).json({ error: "Tasks not found for this user", newToken});
      }
      res.json({tasks, newToken});
    } catch (error) {
      res.status(500).json({ error: error });
    }
  },

  async getOneById(req, res, next) {
    try {
      const id = req.params.id;
      const task = await Task.findById(id);
      if (!task) {
        res.status(404).json("Task not found");
      }
      res.json(task);
    } catch (error) {
      res.status(500).json({ error: error });
    }
  },

  async addOne(req, res) {
    try {

      const taskData = req.body;
      const newTask = await new Task(taskData).save();
      if(req.headers.authorization) {
        const token = req.headers.authorization.replace('Bearer ', '');
        const infos = JSON.parse(Buffer.from(token.split('.')[1], 'base64').toString());
        const newToken = jwt.makeToken(infos.data);
        await dbCache.set("user-0"+infos.data, newToken, {EX: 4*60*60, NX: false});

        if (!newTask) {
          res.status(404).json({ error: "Task not found", newToken});
        }
        res.json({newTask, newToken});
      } else {
        if (!newTask) {
          res.status(404).json({ error: "Task not found"});
        }
        res.json({newTask});
      }

    } catch (error) {
      console.log(error)
      res.status(500).json({ error: error });
    }
  }
};