const bcrypt = require('bcrypt');
const User = require("../models/user");
const jwt = require('../auth/jwt');
const dbCache = require('../auth/cache');

module.exports = {
  async getAllUsers(_, res) {
    try {
      const users = await User.find();
      if (!users) {
        res.status(404).json("users not found");
      }
      res.json(users);
    } catch (error) {
      res.status(500).json({ error: error });
    }
  },

  async getOneById(req, res) {
    try {
      const id = req.params.id;
      const user = await User.findById(id);
      if (!user) {
        res.status(404).json("user not found");
      }
      res.json(user);
    } catch (error) {
      res.status(500).json({ error: error });
    }
  },

  async addOne(req, res, next) {
    try {
      const userData = req.body;
      const foundUser = await User.findOne({ email: userData.email });
      if (foundUser) return res.status(400).send("User already registered.");
      const token = jwt.makeToken(userData.email);
      const password = await bcrypt.hash(userData.password, 10);
      const newUser = await new User({...userData, password}).save();
      if (!newUser) {
        res.status(404).json("user not found");
      }
      await dbCache.set("user-0"+newUser['email'], token, {EX: 4*60*60, NX: false});
      res.json({...newUser['_doc'], token});
    } catch (error) {
      console.log(error)
      res.status(500).json({ error: error });
    }
  },

  async deleteOne(req, res) {
    try {
      const userData = req.body;
      const deletedUser = await User.deleteOne({_id : userData.id})
      res.json(deletedUser);
    } catch (error) {
      console.log(error)
      res.status(500).json({ error: error });
    }
  },

  async login(req, res) {
    const userData = req.body.newUser;
    const user = await User.findOne({email: userData.email});
    const isPwdValid = await bcrypt.compare(userData.password, user.password);
    const password = await bcrypt.hash(userData.password, 10);
    const token = jwt.makeToken(userData.email);
    if (!isPwdValid) {        
      return res.status(404).json("Invalid Password");
    }
    if (!user) {
      return res.status(404).json("user not found");
    }
    await dbCache.set("user-0"+user['email'], token, {EX: 4*60*60, NX: false});
    res.json({...user['_doc'], token});
  }
};