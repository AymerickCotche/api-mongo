const mongoose = require('mongoose');

mongoose.connect(process.env.DATABASE_URL);

const taskSchema = new mongoose.Schema({
  id: String,
  name: String,
  owner: mongoose.Schema.Types.ObjectId
})


module.exports = mongoose.model('task', taskSchema, 'task');