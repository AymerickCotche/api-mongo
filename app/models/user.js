const mongoose = require('mongoose');

mongoose.connect(process.env.DATABASE_URL);

const userSchema = new mongoose.Schema({
  id: String,
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: String,
  name: String
})


module.exports = mongoose.model('user', userSchema, 'user');