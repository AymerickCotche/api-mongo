const { Router } = require('express');

const taskController = require('./controllers/taskController');
const userController = require('./controllers/userController');
const jwtMW = require('./middlewares/jwtMW');

const router = Router();

/**
 * GET /tasks
 * @summary Responds with all tasks in database
 * @route GET /tasks
 * @tags Task
 * @returns {array<Task>} 200 - An array of tasks
 */
router.get("/tasks", taskController.getAllTasks);

/**
 * GET /task/{id}
 * @summary Responds with one task from database
 * @route GET /task/{id}
 * @tags Task
 * @param {string} id.path.required The id of the task to fetch
 * @returns {Task} 200 - A single task identified by its id
 * @returns {string} 404 - An error message
 * 
 */
router.get("/task/:id", taskController.getOneById);
/**
 * GET /tasks/{userId}
 * @summary Responds with all tasks of 1 user from database
 * @route GET /tasks/{userId}
 * @tags Task
 * @param {string} userId.path.required The id of the task to fetch
 * @returns {Task} 200 - A single task identified by its id
 * @returns {string} 404 - An error message
 * 
 */
router.get("/tasks/:userId", jwtMW, taskController.getAllTasksOneUser);
/**
 * POST /task/add
 * @summary Insert one task in the database
 * @route POST /task/add
 * @tags Task
 * @param {TaskJson} request.body.required - application/json
 * @returns {Task} 200 - A single task identified by its id
 * @returns {string} 404 - An error message
 * 
 */
router.post("/task/add", taskController.addOne);

/**
 * GET /users/
 * @summary Responds with all users from database
 * @route GET /task/{id}
 * @tags User
 * @returns {array<User>} 200 - A single task identified by its id
 * @returns {string} 404 - An error message
 * 
 */
router.get("/users", userController.getAllUsers);

/**
 * GET /user/{id}
 * @summary Responds with one user from database
 * @route GET /user/{id}
 * @tags User
 * @param {string} id.path.required The id of the task to fetch
 * @returns {User} 200 - A single user identified by its id
 * @returns {string} 404 - An error message
 * 
 */
router.get("/user/:id", userController.getOneById);
/**
 * POST /user/add
 * @summary Insert one user in the database
 * @route POST /user/add
 * @tags User
 * @param {UserJson} request.body.required - application/json
 * @returns {User} 200 - A single task identified by its id
 * @returns {string} 404 - An error message
 * 
 */
router.post("/user/add", userController.addOne);
/**
 * POST /user/login
 * @summary Get an user of database if email found, it willalso return the JWT token
 * @route POST /user/login
 * @tags User
 * @param {UserJson} request.body.required - application/json
 * @returns {User} 200 - A single user identified by its id
 * @returns {string} 404 - An error message
 * 
 */
router.post("/user/login", userController.login);

/**
 * POST /user/delete
 * @summary Delete an user from database
 * @route POST /user/delete
 * @tags User
 * @param {User} request.body.required - application/json
 * @returns {User} 200 - A single user identified by its id
 * @returns {string} 404 - An error message
 * 
 */
router.post("/user/delete", userController.deleteOne);

/**
 * A task
 * @typedef {object} Task
 * @property {string} id.required - The id
 * @property {string} name - The task name
 * @property {string} owner - The owner id
 * @property {number} version - The owner id
 */

/**
 * An user
 * @typedef {object} User
 * @property {string} id.required - The id
 * @property {string} email - The user email
 * @property {string} password - The owner id
 */

/**
 * Expected json object in request.body
 * @typedef {object} TaskJson
 * @property {string} name
 * @property {string} owner
 */

/**
 * Expected json object in request.body
 * @typedef {object} UserJson
 * @property {string} email
 * @property {string} password
 */

module.exports = router;
