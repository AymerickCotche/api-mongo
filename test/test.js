const { expect } = require('chai');
const request = require('supertest');
const should = require('should');
const app = require('../server');

describe('Test route GET /users', () => {
  it('should return a response with status 200', async () => {
    const response = await request(app)
      .get('/users')
    expect(response.status).equal(200);
  })
  
});

describe('Full test user', () => {

  describe("Create a new User", () => {
    it('should return a response with status 200 and new user infos',  async () => {
      const addResponse = await request(app)
         .post('/user/add')
         .set('Accept','application/json')
         .send({email: 'user8@mail.com', password: 'mdp'})
      expect(addResponse.status).equal(200);

      describe('Get all user', () => {
        it('should return a array of 3 users',  async () => {
          const response = await request(app)
            .get('/users')
          expect(response.body.length).equal(3);
         })
         
      });

      describe("Create a user with a already used email", () => {
        it('should fails and return a stats 500',  async () => {
          const addResponse = await request(app)
            .post('/user/add')
            .set('Accept','application/json')
            .send({email: 'user8@mail.com', password: 'mdp'})
          expect(addResponse.status).equal(400);
        })
      })
       
   
      describe('Test route POST /user/delete', () => {
        it('should return a response with status 200',  async () => {
          const deleteResponse = await request(app)
            .post('/user/delete')
            .set('Accept','application/json')
            .send({id: addResponse.body._id, email: 'user8@mail.com'})
          expect(deleteResponse.status).equal(200);
         })
         
      });

      describe('Get a user with ID', () => {
        it('should return user with email user6@mail.com',  async () => {
          const foundUser = await request(app)
            .get('/user/6618c938533c799877a28faa')
          expect(foundUser.body.email).equal('user6@mail.com');
         })
         
      });

      describe('Get a user with ID', () => {
        it('should not return a user with elodie@mail.com',  async () => {
          const foundUser = await request(app)
            .get('/user/6618c938533c799877a28faa')
          expect(foundUser.body.email).not.equal('elodie@mail.com');
         })
         
      });

      describe('Get all user', () => {
        it('should return a array of 2 users',  async () => {
          const response = await request(app)
            .get('/users')
          expect(response.body.length).equal(2);
         })
         
      });
    })
  })
  
  
});


// describe('Test route POST /user/login', () => {
//   it("should return a response with status 200 and user's infos",  () => {
//     request(app)
//       .post('/user/login')
//       .set('Accept','application/json')
//       .send({email: 'user5@mail.com', password: 'mdp'})
//       .expect('Content-Type', /json/)
//       .expect(201)
//       .end(function(err, res) {
//         res.body._id.should.equal('647982a28ab445253eeadd93');
//         res.body.email.should.equal('user5@mail.com');

//       });
//   })
  
// });